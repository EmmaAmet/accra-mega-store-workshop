package io.turntabl.accramegastore.services.recommendation;

import io.turntabl.accramegastore.services.product.Product;
import io.turntabl.accramegastore.services.product.ProductService;

import java.util.*;

public class RecommendationService {

    private final ProductService productService;
    private final ComplementaryService complementaryService;

    public RecommendationService(ProductService productService, ComplementaryService complementaryService) {
        this.productService = productService;
        this.complementaryService = complementaryService;
    }

    public Optional<Product> getSimilarProduct(String productId) {
        Product thisProduct = productService.getProduct(productId);

        for (Product product : productService.getProducts()) {
            if (!product.getId().equals(thisProduct.getId()) &&
                    product.getStockLevel() > 0 &&
                    product.getCategory().equals(thisProduct.getCategory()) &&
                    product.getName().equals(thisProduct.getName())) {
                return Optional.of(product);
            }
        }
        return Optional.empty();
    }

    public Optional<Product> getComplementary(String productionId) {
        List<String> complements = new ArrayList<>(complementaryService.getComplements(productionId));

        if (!complements.isEmpty()) {
             return Optional.ofNullable(productService.getProduct(complements.get(0)));
        }

        return Optional.empty();
    }
}
